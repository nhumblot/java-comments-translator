package fr.nicolashumblot.java.comments.translator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

class ExtractMultiLinesParserStateTest {

  @Test
  void should_parse_translate_when_called_with_when_the_comment_end() {
    // Given
    String commentLine = "I'm a comment line";
    CommentTranslator translator = mockCommentTranslator(commentLine);

    AppFactory factory = mock(AppFactory.class);

    var state = new ExtractMultiLinesParserState(translator, "/*", factory);

    state.parse(commentLine);

    String endOfComment = "*/";

    // When
    state.parse(endOfComment);

    // Then
    verify(translator).translateText(commentLine);
  }

  private CommentTranslator mockCommentTranslator(String commentLine) {
    CommentTranslator translator = mock(CommentTranslator.class);
    when(translator.translateText(commentLine)).thenReturn(commentLine);
    return translator;
  }

  @Test
  void should_parse_translate_a_comment_if_formatted_on_one_line() {
    // Given
    String comment = "I'm a comment";
    String commentLine = String.format("/* %s */", comment);
    CommentTranslator translator = mockCommentTranslator(comment);

    AppFactory factory = mock(AppFactory.class);

    var state = new ExtractMultiLinesParserState(translator, commentLine, factory);

    // When
    state.parse(commentLine);

    // Then
    verify(translator).translateText(comment);
  }
}
