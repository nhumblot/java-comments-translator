package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translation;
import org.junit.jupiter.api.Test;

class GoogleTranslationApiTest {

  @Test
  void should_translate_return_hello_when_called_with_bonjour() {
    // Given
    String input = "Bonjour";
    String expectedOutput = "Hello";

    Translation translation = mock(Translation.class);
    when(translation.getTranslatedText()).thenReturn(expectedOutput);

    Translate translate = mock(Translate.class);
    when(translate.translate(input)).thenReturn(translation);

    GoogleTranslationApi api = new GoogleTranslationApi(translate);

    // When
    String output = api.translate(input);

    // Then
    assertEquals(expectedOutput, output);
  }
}
