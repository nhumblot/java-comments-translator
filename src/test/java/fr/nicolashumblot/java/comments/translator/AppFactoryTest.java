package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

import java.nio.charset.Charset;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;

class AppFactoryTest {

  @Test
  void should_getArgumentParser_return_instance() {
    // Given
    AppFactory factory = new AppFactory();
    String[] arguments = new String[] {"JavaFile.java"};

    // When
    ArgumentParser output = factory.getArgumentParser(arguments);

    // Then
    assertNotNull(output);
  }

  @Test
  void should_getJavadocParser_return_instance() {
    // Given
    AppFactory factory = new AppFactory();
    Path path = mock(Path.class);
    Charset charset = mock(Charset.class);

    // When
    CommentTranslator output = factory.getCommentTranslator(path, charset);

    // Then
    assertNotNull(output);
  }

  @Test
  void should_getFileCopier_return_instance() {
    // Given
    AppFactory factory = new AppFactory();

    // When
    FileCopier output = factory.getFileCopier();

    // Then
    assertNotNull(output);
  }
}
