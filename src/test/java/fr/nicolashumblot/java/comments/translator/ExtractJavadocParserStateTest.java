package fr.nicolashumblot.java.comments.translator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

class ExtractJavadocParserStateTest {

  @Test
  void should_parse_translate_when_called_with_when_the_comment_end() {
    // Given
    String commentLine = "I'm a comment line";

    CommentTranslator translator = mock(CommentTranslator.class);
    when(translator.translateText(commentLine)).thenReturn(commentLine);

    AppFactory factory = mock(AppFactory.class);

    ExtractJavadocParserState state = new ExtractJavadocParserState(translator, "/**", factory);

    state.parse(commentLine);

    String endOfComment = "*/";

    // When
    state.parse(endOfComment);

    // Then
    verify(translator).translateText(commentLine);
  }
}
