package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.Charset;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;

class AppTest {

  @Test
  void should_launch_return_0_when_called_with_valid_arguments() {
    // Given
    AppFactory factory = createAppFactoryMock();

    App app = new App(factory);
    String[] args = new String[0];

    int expectedOutput = 0;

    // When
    int output = app.tryLaunch(args);

    // Then
    assertEquals(expectedOutput, output);
  }

  private AppFactory createAppFactoryMock() {
    CommentTranslator translator = mock(CommentTranslator.class);
    return createAppFactoryMock(translator);
  }

  private AppFactory createAppFactoryMock(CommentTranslator translator) {
    FileCopier fileCopier = mock(FileCopier.class);

    ArgumentParser argumentParser = mock(ArgumentParser.class);
    when(argumentParser.getPath()).thenReturn(mock(Path.class));
    when(argumentParser.getCharset()).thenReturn(mock(Charset.class));

    return createAppFactoryMock(translator, fileCopier, argumentParser);
  }

  private AppFactory createAppFactoryMock(CommentTranslator translator, FileCopier fileCopier,
                                          ArgumentParser argumentParser) {

    AppFactory factory = mock(AppFactory.class);
    when(factory.getArgumentParser(any(String[].class))).thenReturn(argumentParser);
    when(factory.getCommentTranslator(any(Path.class), any(Charset.class))).thenReturn(translator);
    when(factory.getFileCopier()).thenReturn(fileCopier);
    return factory;
  }

  @Test
  void should_launch_backup_file() {
    // Given
    CommentTranslator translator = mock(CommentTranslator.class);
    FileCopier copier = mock(FileCopier.class);

    String path = "my-path";
    Path parsedPath = Path.of(path);

    ArgumentParser parser = mock(ArgumentParser.class);
    when(parser.getPath()).thenReturn(parsedPath);

    AppFactory factory = createAppFactoryMock(translator, copier, parser);

    App app = new App(factory);
    String[] args = {path};

    // When
    app.tryLaunch(args);

    // Then
    verify(copier).copy(parsedPath);
  }

  @Test
  void should_launch_translate_file() {
    // Given
    CommentTranslator translator = mock(CommentTranslator.class);
    AppFactory factory = createAppFactoryMock(translator);

    App app = new App(factory);
    String[] args = new String[0];

    // When
    app.tryLaunch(args);

    // Then
    verify(translator).translate();
  }

  @Test
  void should_launch_return_1_when_an_exception_occur() {
    // Given
    String[] args = new String[0];

    AppFactory factory = mock(AppFactory.class);
    when(factory.getArgumentParser(args)).thenThrow(IllegalArgumentException.class);

    App app = new App(factory);

    int expectedOutput = 1;

    // When
    int output = app.tryLaunch(args);

    // Then
    assertEquals(expectedOutput, output);
  }
}
