package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class ArgumentParserTest {

  @Test
  void should_getPath_return_the_file_to_parse() {
    // Given
    String[] args = new String[] {"FileToParse.java"};
    ArgumentParser parser = ArgumentParser.create(args);

    Path expectedOutput = Path.of("FileToParse.java");

    // When
    Path output = parser.getPath();

    // Then
    assertEquals(expectedOutput, output);
  }

  @Test
  void should_getPath_return_the_file_to_parse_when_called_with_encoding() {
    // Given
    String[] args = new String[] {"ISO-8859-1", "FileToParse.java"};
    ArgumentParser parser = ArgumentParser.create(args);

    Path expectedOutput = Path.of("FileToParse.java");

    // When
    Path output = parser.getPath();

    // Then
    assertEquals(expectedOutput, output);
  }

  @Test
  void should_getCharset_return_default_charset() {
    // Given
    String[] args = new String[] {"FileToParse.java"};
    ArgumentParser parser = ArgumentParser.create(args);

    Charset expectedOutput = StandardCharsets.UTF_8;

    // When
    Charset output = parser.getCharset();

    // Then
    assertEquals(expectedOutput, output);
  }

  @Test
  void should_getCharset_return_the_charset() {
    // Given
    String[] args = new String[] {"ISO-8859-1", "FileToParse.java"};
    ArgumentParser parser = ArgumentParser.create(args);

    Charset expectedOutput = StandardCharsets.ISO_8859_1;

    // When
    Charset output = parser.getCharset();

    // Then
    assertEquals(expectedOutput, output);
  }

  @Test
  void should_create_throw_IllegalArgumentException_when_called_with_empty_array() {
    // Given
    String[] args = new String[] {};

    // When
    Executable output = () -> ArgumentParser.create(args);

    // Then
    assertThrows(IllegalArgumentException.class, output);
  }

  @Test
  void should_create_throw_IllegalArgumentException_when_called_with_more_than_two_param() {
    // Given
    String[] args = new String[] {"one", "two", "three"};

    // When
    Executable output = () -> ArgumentParser.create(args);

    // Then
    assertThrows(IllegalArgumentException.class, output);
  }
}
