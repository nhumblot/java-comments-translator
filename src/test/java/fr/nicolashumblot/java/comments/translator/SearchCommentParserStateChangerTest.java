package fr.nicolashumblot.java.comments.translator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

class SearchCommentParserStateChangerTest {

  @Test
  void should_Javadoc_changeState_parse_line() {
    // Given
    var stateChanger = SearchCommentParserStateChanger.JAVADOC;

    String line = "Comment line";
    CommentTranslator translator = mock(CommentTranslator.class);
    ExtractJavadocParserState parser = mock(ExtractJavadocParserState.class);

    AppFactory factory = mockAppFactory(line, translator, parser);

    // When
    stateChanger.changeState(line, translator, factory);

    // Then
    verify(parser).parse(line);
  }

  private AppFactory mockAppFactory(String line, CommentTranslator translator,
                                    ExtractJavadocParserState parser) {

    AppFactory factory = mock(AppFactory.class);
    when(factory.getExtractJavadocParserState(translator, line)).thenReturn(parser);
    return factory;
  }

  private AppFactory mockAppFactory(String line, CommentTranslator translator,
                                    ExtractMultiLinesParserState parser) {

    AppFactory factory = mock(AppFactory.class);
    when(factory.getExtractMultiLinesParserState(translator, line)).thenReturn(parser);
    return factory;
  }

  private AppFactory mockAppFactory(CommentTranslator translator,
                                    ExtractSingleLineParserState parser) {

    AppFactory factory = mock(AppFactory.class);
    when(factory.getExtractSingleLineParserState(translator)).thenReturn(parser);
    return factory;
  }

  @Test
  void should_Multi_Lines_changeState_parse_line() {
    // Given
    var stateChanger = SearchCommentParserStateChanger.MULTI_LINES;

    String line = "Comment line";
    CommentTranslator translator = mock(CommentTranslator.class);
    ExtractMultiLinesParserState parser = mock(ExtractMultiLinesParserState.class);

    AppFactory factory = mockAppFactory(line, translator, parser);

    // When
    stateChanger.changeState(line, translator, factory);

    // Then
    verify(parser).parse(line);
  }

  @Test
  void should_Single_Line_changeState_parse_line() {
    // Given
    var stateChanger = SearchCommentParserStateChanger.SINGLE_LINE;

    String line = "Comment line";
    CommentTranslator translator = mock(CommentTranslator.class);
    ExtractSingleLineParserState parser = mock(ExtractSingleLineParserState.class);

    AppFactory factory = mockAppFactory(translator, parser);

    // When
    stateChanger.changeState(line, translator, factory);

    // Then
    verify(parser).parse(line);
  }
}