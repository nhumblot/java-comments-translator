package fr.nicolashumblot.java.comments.translator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;

class ExtractSingleLineParserStateTest {

  @Test
  void should_parse_translate_when_called_with_when_the_comment_end() {
    // Given
    CommentTranslator translator = mock(CommentTranslator.class);

    AppFactory factory = mock(AppFactory.class);

    String comment = "Increment integer";
    String commentLine = "return integer++; // " + comment;

    ExtractSingleLineParserState state =
        new ExtractSingleLineParserState(translator, factory);

    String notACommentLine = "}";

    // When
    state.parse(commentLine);
    state.parse(notACommentLine);

    // Then
    verify(translator).translateText(comment);
  }
}
