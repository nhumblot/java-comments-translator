package fr.nicolashumblot.sample;

/**
 * This is a basic Javadoc comment.
 */
public class JavadocTestFile {

  /**
   * This comment is a multi-line
   * javadoc comment.
   */
  public static final String A_FIELD;

  /**This comment must be translated*/
  public static final int AN_INTEGER;

  /**
   * Increments the integer in parameter.
   *
   * @param integer to increment
   * @return an integer equals to the integer in parameter incremented
   */
  public int increment(int integer) {
    /*
     * This is a multi lines comment,
     * we need to parse these also!
     */
    return integer++; // Increment integer
  }

  public String sayHello() {
    /* Hello world! */
    return "Hello World!";
  }
}
