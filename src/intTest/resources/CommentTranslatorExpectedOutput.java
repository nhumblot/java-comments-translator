package fr.nicolashumblot.sample;

/**
 * Ceci une Javadoc basique.
 */
public class JavadocTestFile {

  /**
   * Ce commentaire est une Javadoc sur plusieurs lignes.
   */
  public static final String A_FIELD;

  /**
   * Ce commentaire doit être traduit
   */
  public static final int AN_INTEGER;

  /**
   * Incrémente l'entier en argument.
   * @param l'entier à incrémenter
   * @return un entier égal à l'entier en argument incrémenté
   */
  public int increment(int integer) {
    /*
     * Ceci est un commentaire sur plusieurs lignes, nous devons l'analyser également !
     */
    return integer++; // Incrémente l'entier
  }

  public String sayHello() {
    /*
     * Bonjour le monde !
     */
    return "Hello World!";
  }
}
