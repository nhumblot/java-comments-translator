package fr.nicolashumblot.java.comments.translator;

class AppFactoryStub extends AppFactory {

  private final TranslationApiStub translationApi;

  AppFactoryStub(TranslationApiStub translationApi) {
    this.translationApi = translationApi;
  }

  @Override
  TranslationApi getTranslationApi() {
    return translationApi;
  }
}
