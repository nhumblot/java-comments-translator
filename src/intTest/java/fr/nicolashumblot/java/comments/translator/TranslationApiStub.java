package fr.nicolashumblot.java.comments.translator;

import java.util.Map;

class TranslationApiStub implements TranslationApi {

  private final Map<String, String> translations;

  TranslationApiStub(Map<String, String> translations) {
    this.translations = translations;
  }

  @Override
  public String translate(String input) {
    return translations.computeIfAbsent(input, key -> key);
  }
}
