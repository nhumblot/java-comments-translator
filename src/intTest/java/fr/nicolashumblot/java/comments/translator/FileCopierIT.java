package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class FileCopierIT {

  @Test
  void should_copy_create_a_new_file_with_bk_extension() throws IOException {
    // Given
    String projectDir = System.getProperty("user.dir");
    String filePath = "src/intTest/resources/FileCopierWithoutBackup.java.bk";
    Path backupPath = Path.of(projectDir, filePath);

    if (Files.exists(backupPath)) {
      Files.delete(backupPath);
    }

    Path path = Path.of(projectDir, "src/intTest/resources/FileCopierWithoutBackup.java");
    FileCopier fileCopier = new FileCopier();

    // When
    fileCopier.copy(path);

    // Then
    assertTrue(Files.exists(backupPath));
  }

  @Test
  void should_copy_throws_UncheckedIoException_when_backup_already_exists() {
    // Given
    String projectDir = System.getProperty("user.dir");
    Path path = Path.of(projectDir, "src/intTest/resources/FileCopierWithBackup.java");

    FileCopier fileCopier = new FileCopier();

    // When
    Executable executable = () -> fileCopier.copy(path);

    // Then
    assertThrows(UncheckedIOException.class, executable);
  }
}
