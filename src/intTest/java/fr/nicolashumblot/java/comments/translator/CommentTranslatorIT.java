package fr.nicolashumblot.java.comments.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CommentTranslatorIT {

  @BeforeEach
  void backup() throws IOException {
    String projectDir = System.getProperty("user.dir");
    Path path = Path.of(projectDir, "src/intTest/resources/CommentTranslator.java");
    Path backup = Path.of(projectDir, "src/intTest/resources/CommentTranslator.java.bk");
    Files.copy(path, backup);
  }

  @AfterEach
  void restore() throws IOException {
    String projectDir = System.getProperty("user.dir");
    Path path = Path.of(projectDir, "src/intTest/resources/CommentTranslator.java");
    Path backup = Path.of(projectDir, "src/intTest/resources/CommentTranslator.java.bk");
    Files.copy(backup, path, StandardCopyOption.REPLACE_EXISTING);
    Files.deleteIfExists(backup);
  }

  @Test
  void should_translate_replace_comments_in_file() throws IOException {
    // Given
    AppFactory factory = buildFactoryStub();

    String projectDir = System.getProperty("user.dir");
    Path path = Path.of(projectDir, "src/intTest/resources/CommentTranslator.java");
    Charset charset = Charset.defaultCharset();

    CommentTranslator translator = new CommentTranslator(path, charset, factory);

    List<String> expectedOutput = getExpectedOutput();

    // When
    translator.translate();
    List<String> output = Files.readAllLines(path);

    // Then
    assertEquals(expectedOutput, output);
  }

  private AppFactory buildFactoryStub() {
    Map<String, String> translations = prepareTranslations();
    TranslationApiStub translationApi = new TranslationApiStub(translations);
    return new AppFactoryStub(translationApi);
  }

  private Map<String, String> prepareTranslations() {
    Map<String, String> translations = new HashMap<>();
    translations.put("This is a basic Javadoc comment.", "Ceci une Javadoc basique.");

    translations.put("This comment is a multi-line javadoc comment.",
        "Ce commentaire est une Javadoc sur plusieurs lignes.");

    translations.put("This comment must be translated", "Ce commentaire doit être traduit");

    translations.put("Increments the integer in parameter.\n"
            + "@param integer to increment\n"
            + "@return an integer equals to the integer in parameter incremented",
        "Incrémente l'entier en argument.\n@param l'entier à incrémenter\n"
            + "@return un entier égal à l'entier en argument incrémenté");

    translations.put("This is a multi lines comment, we need to parse these also!",
        "Ceci est un commentaire sur plusieurs lignes, nous devons l'analyser également !");

    translations.put("Increment integer", "Incrémente l'entier");

    translations.put("Hello world!", "Bonjour le monde !");
    return translations;
  }

  private List<String> getExpectedOutput() throws IOException {
    String projectDir = System.getProperty("user.dir");
    Path path = Path.of(projectDir, "src/intTest/resources/CommentTranslatorExpectedOutput.java");
    return Files.readAllLines(path);
  }
}
