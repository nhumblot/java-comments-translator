package fr.nicolashumblot.java.comments.translator;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

  private static final Logger LOGGER = Logger.getLogger(App.class.getName());

  private final AppFactory factory;

  App() {
    factory = new AppFactory();
  }

  App(AppFactory factory) {
    this.factory = factory;
  }

  public static void main(String[] args) {
    var app = new App();
    app.tryLaunch(args);
  }

  int tryLaunch(String[] args) {
    try {
      return launch(args);
    } catch (RuntimeException e) {
      LOGGER.log(Level.SEVERE, "Unexpected error", e);
      return 1;
    }
  }

  private int launch(String[] args) {
    var parser = factory.getArgumentParser(args);

    var path = parser.getPath();
    var charset = parser.getCharset();

    backupFile(path);
    translateFile(path, charset);

    return 0;
  }

  private void backupFile(Path path) {
    var fileCopier = factory.getFileCopier();
    fileCopier.copy(path);
  }

  private void translateFile(Path path, Charset charset) {
    var translator = factory.getCommentTranslator(path, charset);
    translator.translate();
  }
}
