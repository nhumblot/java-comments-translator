package fr.nicolashumblot.java.comments.translator;

class SearchCommentParserState implements CommentParserState {

  private final CommentTranslator translator;
  private final AppFactory factory;

  SearchCommentParserState(CommentTranslator translator, AppFactory factory) {
    this.translator = translator;
    this.factory = factory;
  }

  @Override
  public void parse(String line) {
    SearchCommentParserStateChanger.of(line)
        .ifPresentOrElse(changer -> changer.changeState(line, translator, factory),
            () -> translator.writeLine(line));
  }

}
