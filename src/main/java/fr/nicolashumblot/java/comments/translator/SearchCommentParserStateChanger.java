package fr.nicolashumblot.java.comments.translator;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

enum SearchCommentParserStateChanger {

  JAVADOC((line, translator, factory) -> {
    CommentParserState newState = factory.getExtractJavadocParserState(translator, line);
    translator.setState(newState);
    newState.parse(line);
  },
      line -> line.contains("/**")
  ),
  MULTI_LINES((line, translator, factory) -> {
    CommentParserState newState = factory.getExtractMultiLinesParserState(translator, line);
    translator.setState(newState);
    newState.parse(line);
  },
      line -> line.contains("/*")),
  SINGLE_LINE((line, translator, factory) -> {
    CommentParserState newState = factory.getExtractSingleLineParserState(translator);
    translator.setState(newState);
    newState.parse(line);
  },
      line -> line.contains("//")
  );

  private final TriConsumer<String, CommentTranslator, AppFactory> changeState;
  private final Function<String, Boolean> isPresent;

  SearchCommentParserStateChanger(TriConsumer<String, CommentTranslator, AppFactory> changeState,
                                  Function<String, Boolean> isPresent) {

    this.changeState = changeState;
    this.isPresent = isPresent;
  }

  static Optional<SearchCommentParserStateChanger> of(String line) {
    return Arrays.stream(SearchCommentParserStateChanger.values())
        .filter(comment -> comment.isPresent(line)).findFirst();
  }

  private boolean isPresent(String line) {
    return isPresent.apply(line);
  }

  void changeState(String line, CommentTranslator translator, AppFactory factory) {
    changeState.accept(line, translator, factory);
  }
}
