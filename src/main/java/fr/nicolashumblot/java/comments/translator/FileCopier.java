package fr.nicolashumblot.java.comments.translator;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

class FileCopier {

  void copy(Path path) {
    var backupPath = pickBackupPath(path);
    tryCopyFile(path, backupPath);
  }

  private Path pickBackupPath(Path path) {
    String backupFileName = path.getFileName() + ".bk";
    var parentFolder = path.getParent().toString();
    return Path.of(parentFolder, backupFileName);
  }

  private void tryCopyFile(Path path, Path backupPath) {
    try {
      Files.copy(path, backupPath);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
