package fr.nicolashumblot.java.comments.translator;

interface TranslationApi {

  String translate(String input);

}
