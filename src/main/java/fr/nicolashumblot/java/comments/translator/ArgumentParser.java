package fr.nicolashumblot.java.comments.translator;

import java.nio.charset.Charset;
import java.nio.file.Path;

class ArgumentParser {

  private final Path path;
  private final Charset charset;

  private ArgumentParser(Path path, Charset charset) {
    this.path = path;
    this.charset = charset;
  }

  static ArgumentParser create(String[] args) {
    if (isEmptyOrGreaterThanTwo(args)) {
      throw new IllegalArgumentException("Invalid number of arguments");
    }

    var path = extractPath(args);
    var charset = extractCharset(args);

    return new ArgumentParser(path, charset);
  }

  private static boolean isEmptyOrGreaterThanTwo(String[] args) {
    return args.length == 0 || args.length > 2;
  }

  private static String getLastElement(String[] args) {
    return args[args.length - 1];
  }

  private static Path extractPath(String[] args) {
    String lastElement = getLastElement(args);
    return Path.of(lastElement);
  }

  private static Charset extractCharset(String[] args) {
    var charset = Charset.defaultCharset();
    if (args.length == 2) {
      charset = Charset.forName(args[0]);
    }
    return charset;
  }

  public Path getPath() {
    return path;
  }

  public Charset getCharset() {
    return charset;
  }
}
