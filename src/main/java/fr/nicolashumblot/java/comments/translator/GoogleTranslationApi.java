package fr.nicolashumblot.java.comments.translator;

import com.google.cloud.translate.Translate;

class GoogleTranslationApi implements TranslationApi {

  private final Translate translate;

  GoogleTranslationApi(Translate translate) {
    this.translate = translate;
  }

  @Override
  public String translate(String input) {
    var translation = this.translate.translate(input);
    return translation.getTranslatedText();
  }
}
