package fr.nicolashumblot.java.comments.translator;

import java.util.ArrayList;
import java.util.List;

class ExtractSingleLineParserState implements CommentParserState {

  private final CommentTranslator translator;
  private final List<ExtractedComment> extractedLines;
  private final AppFactory factory;

  ExtractSingleLineParserState(CommentTranslator translator, AppFactory factory) {
    this.translator = translator;
    extractedLines = new ArrayList<>();
    this.factory = factory;
  }

  @Override
  public void parse(String line) {
    if (containsComment(line)) {
      ExtractedComment comment = extractComment(line);
      extractedLines.add(comment);
    } else {
      writeCommentsAndCurrentLine(line);

      CommentParserState newState = factory.getSearchCommentParserState(translator);
      translator.setState(newState);
    }
  }

  private boolean containsComment(String line) {
    return line.contains("//");
  }

  private ExtractedComment extractComment(String line) {
    String[] splitted = line.split("//", 2);
    String prefix = splitted[0];
    String comment = splitted[1].trim();
    return new ExtractedComment(prefix, comment);
  }

  private void writeCommentsAndCurrentLine(String line) {
    extractedLines.forEach(extractedComment -> {
      String translatedComment = translator.translateText(extractedComment.getComment());
      translator.writeLine(extractedComment.getPrefix() + "// " + translatedComment);
    });

    translator.writeLine(line);
  }
}
