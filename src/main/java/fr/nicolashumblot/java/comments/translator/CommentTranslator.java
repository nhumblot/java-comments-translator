package fr.nicolashumblot.java.comments.translator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class CommentTranslator {

  private final List<String> newFile = new ArrayList<>();
  private final Path path;
  private final Charset charset;
  private CommentParserState state;
  private final TranslationApi translationApi;

  CommentTranslator(Path path, Charset charset, AppFactory factory) {
    this.path = path;
    this.charset = charset;
    this.state = factory.getSearchCommentParserState(this);
    this.translationApi = factory.getTranslationApi();
  }

  void translate() {
    try (var reader = Files.newBufferedReader(path, charset)) {
      doTranslate(reader);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private void doTranslate(BufferedReader reader) throws IOException {
    for (String line = reader.readLine(); Objects.nonNull(line); line = reader.readLine()) {
      state.parse(line);
    }
    Files.write(path, newFile, Charset.defaultCharset());
  }

  void setState(CommentParserState state) {
    this.state = state;
  }

  String translateText(String comment) {
    return translationApi.translate(comment);
  }

  void writeLine(String line) {
    newFile.add(line);
  }
}
