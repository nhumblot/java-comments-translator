package fr.nicolashumblot.java.comments.translator;

import java.util.List;

class ExtractJavadocParserState extends AbstractMultiLinesParserState {

  private static final String COMMENT_BEGIN_SYNTAX = "/**";

  ExtractJavadocParserState(CommentTranslator translator, String line, AppFactory factory) {
    super(translator, line, COMMENT_BEGIN_SYNTAX, factory);
  }

  @Override
  String joinExtractedLines(List<String> extractedLines) {
    var accumulator = new StringBuilder();

    extractedLines.forEach(l -> {
      if (accumulator.length() > 0 && l.startsWith("@")) {
        accumulator.append("\n");
      } else if (accumulator.length() > 0) {
        accumulator.append(" ");
      }

      accumulator.append(l);
    });

    return accumulator.toString();
  }

  @Override
  String trimAndRemoveCommentFormatting(String line) {
    if (line.trim().startsWith(COMMENT_BEGIN_SYNTAX)) {
      line = line.replaceFirst("/\\*\\*", "");
    }
    return super.trimAndRemoveCommentFormatting(line);
  }
}
