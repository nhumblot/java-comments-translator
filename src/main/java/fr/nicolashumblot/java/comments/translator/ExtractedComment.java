package fr.nicolashumblot.java.comments.translator;

class ExtractedComment {

  private final String prefix;
  private final String comment;

  ExtractedComment(String prefix, String comment) {
    this.prefix = prefix;
    this.comment = comment;
  }

  String getPrefix() {
    return prefix;
  }

  String getComment() {
    return comment;
  }
}
