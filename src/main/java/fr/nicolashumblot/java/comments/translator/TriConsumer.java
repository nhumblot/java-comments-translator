package fr.nicolashumblot.java.comments.translator;

@FunctionalInterface
interface TriConsumer<T, U, V> {

  void accept(T t, U u, V v);
}
