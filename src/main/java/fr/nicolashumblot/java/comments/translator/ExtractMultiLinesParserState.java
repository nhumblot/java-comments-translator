package fr.nicolashumblot.java.comments.translator;

import java.util.List;

class ExtractMultiLinesParserState extends AbstractMultiLinesParserState {

  private static final String COMMENT_BEGIN_SYNTAX = "/*";

  ExtractMultiLinesParserState(CommentTranslator translator, String line, AppFactory factory) {
    super(translator, line, COMMENT_BEGIN_SYNTAX, factory);
  }

  @Override
  String joinExtractedLines(List<String> extractedLines) {
    return String.join(" ", extractedLines);
  }

  @Override
  String trimAndRemoveCommentFormatting(String line) {
    if (line.trim().startsWith(COMMENT_BEGIN_SYNTAX)) {
      line = line.replaceFirst("/\\*", "");
    }
    return super.trimAndRemoveCommentFormatting(line);
  }
}
