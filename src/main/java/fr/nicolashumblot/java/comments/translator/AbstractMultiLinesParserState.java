package fr.nicolashumblot.java.comments.translator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class AbstractMultiLinesParserState implements CommentParserState {

  private final List<String> extractedLines;
  private final String prefix;
  private final String commentBeginSyntax;
  private final CommentTranslator translator;
  private final AppFactory factory;

  AbstractMultiLinesParserState(CommentTranslator translator, String line,
                                String commentBeginSyntax, AppFactory factory) {

    this.extractedLines = new ArrayList<>();
    this.prefix = getPrefix(line);
    this.commentBeginSyntax = commentBeginSyntax;
    this.translator = translator;
    this.factory = factory;
  }

  private String getPrefix(String line) {
    for (var i = 0; i < line.length(); i++) {
      var character = line.charAt(i);
      if (!Character.isWhitespace(character)) {
        return line.substring(0, i);
      }
    }
    return line;
  }

  @Override
  public void parse(String line) {
    extractComment(line);

    if (isCommentEnd(line)) {
      String comment = joinExtractedLines(extractedLines);

      String translatedComment = translator.translateText(comment);
      writeComment(translatedComment);

      CommentParserState newState = factory.getSearchCommentParserState(translator);
      translator.setState(newState);
    }
  }

  private boolean isCommentEnd(String line) {
    return line.contains("*/");
  }

  abstract String joinExtractedLines(List<String> extractedLines);

  private void writeComment(String translatedComment) {
    translator.writeLine(prefix + commentBeginSyntax);

    Arrays.stream(translatedComment.split("\n"))
        .forEach(translatedLine -> translator.writeLine(prefix + " * " + translatedLine));

    translator.writeLine(prefix + " */");
  }

  private void extractComment(String line) {
    String extractedLine = trimAndRemoveCommentFormatting(line);

    if (!extractedLine.isEmpty()) {
      extractedLines.add(extractedLine);
    }
  }

  String trimAndRemoveCommentFormatting(String line) {
    return line.replaceFirst("\\*/", "")
        .replaceFirst("\\*", "")
        .trim();
  }

}
