package fr.nicolashumblot.java.comments.translator;

import com.google.cloud.translate.TranslateOptions;
import java.nio.charset.Charset;
import java.nio.file.Path;

class AppFactory {

  ArgumentParser getArgumentParser(String[] arguments) {
    return ArgumentParser.create(arguments);
  }

  CommentTranslator getCommentTranslator(Path path, Charset charset) {
    return new CommentTranslator(path, charset, this);
  }

  TranslationApi getTranslationApi() {
    var translate = TranslateOptions.getDefaultInstance().getService();
    return new GoogleTranslationApi(translate);
  }

  SearchCommentParserState getSearchCommentParserState(CommentTranslator commentTranslator) {
    return new SearchCommentParserState(commentTranslator, this);
  }

  ExtractJavadocParserState getExtractJavadocParserState(CommentTranslator commentTranslator,
                                                         String line) {
    return new ExtractJavadocParserState(commentTranslator, line, this);
  }

  ExtractMultiLinesParserState getExtractMultiLinesParserState(CommentTranslator commentTranslator,
                                                               String line) {
    return new ExtractMultiLinesParserState(commentTranslator, line, this);
  }

  ExtractSingleLineParserState getExtractSingleLineParserState(
      CommentTranslator commentTranslator) {

    return new ExtractSingleLineParserState(commentTranslator, this);
  }

  FileCopier getFileCopier() {
    return new FileCopier();
  }
}
