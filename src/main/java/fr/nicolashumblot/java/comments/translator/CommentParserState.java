package fr.nicolashumblot.java.comments.translator;

interface CommentParserState {

  void parse(String line);
}
