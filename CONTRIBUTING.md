# How to contribute

Third-party patches are welcomed for improving this application. There are a
few guidelines that we need contributors to follow.

## Getting Started

* Make sure you have a [GitLab account](https://gitlab.com/).
* Submit a GitLab issue for your issue if one does not already exist.
  * Clearly describe the issue including steps to reproduce when it is a bug.
  * Make sure you fill in the earliest version that you know has the issue.
* Fork the repository on GitLab.

## Making Changes

* Create a topic branch from where you want to base your work.
  * This is usually the `develop` branch.
  * Only target release branches if you are certain your fix must be on that
    branch.
  * To quickly create a topic branch based on `develop`, run `git checkout -b
    issueref-my-contribution develop`. Please avoid working directly on
    the `develop` branch.
* Make commits of logical and atomic units.
* Verify that a gradle build is green for each commit.
* Check for unnecessary whitespace with `git diff --check` before committing.
* Make sure your commit messages are in the proper format. If the commit
  addresses an issue filed in the
  [Gitlab project](https://gitlab.com/nhumblot/java-comments-translator), start
  the first line with the issue number.
  ```
  #4 - Make the example in CONTRIBUTING.md imperative and concrete

  Without this patch applied the example commit message in the CONTRIBUTING
  document is not a concrete example. This is a problem because the
  contributor is left to imagine what the commit message should look like
  based on a description rather than an example. This patch fixes the problem
  by making the example concrete and imperative.

  The first line is a real-life imperative statement with an issue number
  from our issue tracker. The body describes the behavior without the patch,
  why this is a problem, and how the patch fixes the problem when applied.
  ```
* Make sure you have added the necessary tests for your changes.

## Making Trivial Changes

For changes of a trivial nature, it is not always necessary to create a new
issue in GitLab. In this case, it is appropriate to start the first line of a
commit with one of `docs`, `maint`, or `packaging` instead of an issue number.

```
docs - Add docs commit example to CONTRIBUTING

There is no example for contributing a documentation commit to the project
repository. This is a problem because the contributor is left to assume how a
commit of this nature may appear.

The first line is a real-life imperative statement with 'docs' in place of
what would have been the project issue number in a non-documentation related
commit. The body describes the nature of the new documentation or comments
added.
```

For commits that address trivial repository maintenance tasks or packaging
issues, start the first line of the commit with `maint` or `packaging`,
respectively.

## Submitting Changes

* Push your changes to a topic branch in your fork of the repository.
* Submit a merge request to the project repository.
* The core team looks at merge requests on a regular basis.
* After feedback has been given we expect responses within a month. After a
  month we may close the merge request if it isn't showing any activity.

## Revert Policy

By running tests in advance and by engaging with peer review for prospective
changes, your contributions have a high probability of becoming long-lived
parts of the project. After being merged, the code will run through a
testing pipeline. These pipelines can reveal incompatibilities that are
difficult to detect in advance.

If the code change results in a test failure, we will make our best effort to
correct the error. If a fix cannot be determined and committed within 24 hours
of its discovery, the commit(s) responsible _may_ be reverted, at the
discretion of the maintainers. This action would be taken to help maintain
passing states in our testing pipeline.

The original contributor will be notified of the revert in the GitLab issue
associated with the change. A reference to the test(s) that failed as a result
of the code change will also be added to the GitLab issue. This test(s) should
be used to check future submissions of the code to ensure the issue has been
resolved.

### Summary

* Changes resulting in test pipeline failures will be reverted if they cannot
  be resolved within one business day.

## Additional Resources

[sonarcloud project](https://sonarcloud.io/dashboard?id=nhumblot_java-comments-translator)
