# Java comments translator

Java command line program translating comments and Javadoc of Java sources.

## Requirements

Java Development Kit 11 (for installation)

Java Runtime Environment 11 (for usage)

## Installation
1. Clone the [git repository](https://gitlab.com/nhumblot/java-comments-translator)
2. Run `./gradlew build` (Unix) or `./gradlew.bat build` (Windows)
3. Jar files are in `build/libs`
4. For a command line execution, please use `java-comments-translator-app.jar`, which is a fat jar.

## Usage

```
export GOOGLE_APPLICATION_CREDENTIALS=<path_to_your_google_api_file>
java -jar java-comments-translator-app.jar FileToTranslate.java
```

If you want to force the encoding of the file, you must call it like this:
```
java -jar java-comments-translator-app.jar ISO-8859-1 FileToTranslate.java
```

## Contributing
Merge requests are welcome. Please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

More information in [CONTRIBUTING.md](CONTRIBUTING.md)

## License
[MIT](https://choosealicense.com/licenses/mit/)