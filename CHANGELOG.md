# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.4

### Fixed
- Upgrade dependencies

## 1.0.3

### Fixed
- Upgrade dependencies

## 1.0.2

### Fixed
- Use a custom task for packaging instead of shadowJar
- Upgrade dependencies

## 1.0.1

### Fixed
- Fix parsing a one line comment with a multi-line syntax
- Upgrade dependencies

## 1.0.0

### Added
- Declare sources under MIT licence
- A basic documentation about the project
- A continuous integration
- Parse javadoc, multi lines and single line comments